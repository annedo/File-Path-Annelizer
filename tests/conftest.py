import pytest
import tempfile
import shutil
from pathlib import Path, PurePath

def destroy_mockdir(path):
    if Path(path).exists():
        shutil.rmtree(path)

def purepath_isdir(self):
    """Tests if a path would be a dir, if it existed."""
    return self.suffix == ''

def pytest_sessionstart():
    PurePath.is_dir = purepath_isdir
    PurePath.is_file =lambda x: not purepath_isdir(x)

    prefix = 'fp_annelizer_pytest__'

    temp_dir = Path('mockdirs')
    if not temp_dir.exists():
        temp_dir.mkdir()

    MOCKDIR = tempfile.mkdtemp(prefix=prefix, dir=temp_dir)
    pytest.MOCKDIR = Path(MOCKDIR)

    try:
        for i in range(1, 4):
            tmp_main = Path(pytest.MOCKDIR) / f'mainfile-{i}.txt'
            tmp_main.touch(exist_ok=True)
            subdir = Path(pytest.MOCKDIR) / f'subdir-{i}'
            subdir.mkdir(exist_ok=True)
            for f in range(i):
                tmp_sub = Path(subdir) / f'subfile-{f+1}.txt'
                tmp_sub.touch(exist_ok=True)
                tmp_sub.write_text('TEST TEXT FILE')

    except Exception as e:
        destroy_mockdir(pytest.MOCKDIR)
        print(e)

def pytest_sessionfinish(session, exitstatus):
    if exitstatus != 1:
        shutil.rmtree(pytest.MOCKDIR)

def pytest_runtest_makereport(item, call):
    if "incremental" in item.keywords:
        if call.excinfo is not None:
            parent = item.parent
            parent._previousfailed = item

def pytest_runtest_setup(item):
    if "incremental" in item.keywords:
        previousfailed = getattr(item.parent, "_previousfailed", None)
        if previousfailed is not None:
            pytest.xfail("previous test failed (%s)" % previousfailed.name)

# SETUP FUNCTIONS AND FIXTURES
## See conftest.py for configured pytest variables and generation of MOCKDIR.
def get_all_test_paths():
    mockdir_children = Path(pytest.MOCKDIR).glob('**/*')
    rel_children = [path for path in mockdir_children]
    abs_children = [path.resolve() for path in rel_children]
    return [*rel_children, *abs_children]

def get_test_files():
    """Return pathlib.Path to all files in pytest.MOCKDIR"""
    return [path for path in get_all_test_paths() if path.is_file()]

def get_test_folders():
    """Return pathlib.Path to all folders in pytest.MOCKDIR"""
    return [path for path in get_all_test_paths() if path.is_dir()]

def get_path_ids(path_list):
    """Mark relative paths and shrink path relative to pytest.MOCKDIR"""
    id_list = []
    for path in path_list:
        if PurePath(path).is_absolute():
            path_id = f'ABSPATH: {path}'
        else: 
            path_id = f'RELPATH: {str(PurePath(path).relative_to(Path(pytest.MOCKDIR)))}'
        id_list.append(path_id)
    return id_list

def pytest_generate_tests(metafunc):
    if 'file_path' in metafunc.fixturenames:
        metafunc.parametrize('file_path', get_test_files(), ids=get_path_ids(get_test_files()))

    if 'folder_path' in metafunc.fixturenames:
        metafunc.parametrize('folder_path', get_test_folders(), ids=get_path_ids(get_test_folders()))

    if 'test_path' in metafunc.fixturenames:
        metafunc.parametrize('test_path', get_all_test_paths(), ids=get_path_ids(get_all_test_paths()))