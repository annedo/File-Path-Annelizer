from _context import fp_annelizer
from fp_annelizer import *

from unittest.mock import Mock
from pathlib import Path, PurePath

import pytest

# SETUP FIXTURES AND MOCKS
@pytest.fixture(scope='class')
def monkeyclass():
    from _pytest.monkeypatch import MonkeyPatch
    mpatch = MonkeyPatch()
    yield mpatch
    mpatch.undo()

@pytest.fixture(scope='class')
def mock_navigate_paths_by_index(monkeyclass):
    monkeyclass.setattr('fp_annelizer.navigate_paths_by_index', lambda *args, **kwargs: False)
    return False

# BEGIN TEST SUITE
class Test_InputListIndex:
    """Tests input_list_index() function with mock temp dir.

    Required Arguments:
        item_list (list): List of options to prompt user selection.

    Optional Arguments:
        None.
    """
    @pytest.fixture(scope='class')
    def TEST_FUNC(self):
        return input_list_index

    @pytest.fixture(scope='class')
    def TEST_LIST(self):
        return ['a', 'b', 'c', 1, 2, 3]

    def test_index_valid(self, TEST_FUNC, TEST_LIST, monkeypatch):
        """Test that valid index input returns as is."""
        with monkeypatch.context() as m:
            for index, item in enumerate(TEST_LIST):
                m.setattr('builtins.input', Mock(return_value=index))
                assert TEST_FUNC(TEST_LIST) == index
                assert TEST_LIST[TEST_FUNC(TEST_LIST)] == TEST_LIST[index]

    def test_index_invalid(self, TEST_FUNC, TEST_LIST, monkeypatch):
        """Test that function calls itself until index input is valid."""
        valid_input = 0
        index_inputs = [-5, len(TEST_LIST) + 5, 'abc', valid_input]
        input_mock = Mock(side_effect=index_inputs, return_value=index_inputs)
        with monkeypatch.context() as m:
            m.setattr('builtins.input', input_mock)
            assert TEST_FUNC(TEST_LIST) == valid_input

@pytest.mark.incremental
class Test_InputFilePath:
    """Test input_file_path() function with relative and absolute paths.

    Required Arguments:
        None.

    Optional Arguments:
        path (str or pathlib.Path): Initial path to parse.
        accept_dir (boolean): Accept paths to valid directory.
        input_message (str): Change generic instructions for input path prompt.
        navigation_message(str): Change generic instructions for navigating subfolders.

    """
    @pytest.fixture(scope='class')
    def TEST_FUNC(self):
        return input_file_path

    def test_valid_file_paths(self, TEST_FUNC, file_path):
        """Test valid rel/abs file paths are returned as is, of type pathlib.Path."""
        assert TEST_FUNC(path=file_path) == file_path
        assert isinstance(TEST_FUNC(path=file_path), Path)

    def test_invalid_file_paths(self, TEST_FUNC, monkeypatch):
        """Test that function calls itself when given invalid path input."""
        valid_file_path = Path(pytest.MOCKDIR) / 'mainfile-1.txt'
        input_series = [None, None, 'thispathdoesnotexist1234.txt', valid_file_path]
        input_mock = Mock(side_effect=input_series, return_value=input_series)
        monkeypatch.setattr('builtins.input', input_mock)
        assert TEST_FUNC() == valid_file_path
        # Mock should have been called without having to do a manual test loop
        assert input_mock.call_count == len(input_series)

@pytest.mark.incremental
class Test_InputPath:
    """Test input_path() function with relative and absolute paths.

    Required Arguments:
        criteria (function): Function to be called on path. Should return True if path is valid.
        Default criteria only checks if path exists on filesystem.

    Optional Arguments:
        path (str|pathlib.Path): Initial path to parse.
        accept_dir (boolean): Accept paths to valid directory.
        input_message (str): Change generic instructions for input path prompt.
        navigation_message(str): Change generic instructions for navigating subfolders.

    """
    @pytest.fixture(scope='class')
    def TEST_FUNC(self):
        return input_path


    def test_criteria_filename(self, TEST_FUNC, monkeypatch, test_path, mock_navigate_paths_by_index):
        """Accept only paths with 0 in the pathlib.Path.name."""
        def path_criteria(path):
            return '0' in path.name

        if path_criteria(test_path):
            assert TEST_FUNC(path_criteria, test_path) == test_path
        else:
            assert TEST_FUNC(path_criteria, test_path) == mock_navigate_paths_by_index

    def test_criteria_isdir(self, TEST_FUNC, monkeypatch, test_path, mock_navigate_paths_by_index):
        """Accept only paths to directories."""
        def path_criteria(path):
            return path.is_dir()

        valid_path = test_path if path_criteria(test_path) else mock_navigate_paths_by_index
        assert TEST_FUNC(path_criteria, test_path) == valid_path

    def test_criteria_containsfilenames(self, TEST_FUNC, monkeypatch, test_path, mock_navigate_paths_by_index):
        """Accept only paths to directories containing file 2.txt"""
        def path_criteria(path):
            try:
                dir_contents = [child for child in path.iterdir()]
                content_matches = list(map(lambda x: '2.txt' in x.name, dir_contents))
                return any(content_matches)
            except NotADirectoryError:
                return False

        valid_path = test_path if path_criteria(test_path) else mock_navigate_paths_by_index
        assert TEST_FUNC(path_criteria, test_path) == valid_path

@pytest.mark.incremental
class Test_NavigatePathsByIndex:
    """Test navigate_paths_by_index() function using pytest.MOCKDIR path.

    Required Arguments:
        dir_path (str|pathlib.Path): Path to folder to navigate within.

    Optional Arguments:
        options_dict (dict): Displays dict.keys() in addition to file contents for selection, 
                             then returns corresponding value. 
                             If value is a Path to a file, returns file.
                             If value is a Path to a folder, calls self with the folder path as dir_path.
                             If value is a function, performs the function.
        navigation_message(str): Change generic instructions for navigating subfolders.

    """
    @pytest.fixture(scope='class')
    def TEST_FUNC(self):
        return navigate_paths_by_index

    def test_file_select(self, TEST_FUNC, monkeypatch):
        dir_contents = [path for path in pytest.MOCKDIR.iterdir()]
        dir_contents.sort()
        index_of_firstfile = 0
        for index, path in enumerate(dir_contents):
            if path.is_file():
                index_of_firstfile = index
                break

        monkeypatch.setattr('fp_annelizer.input_list_index', lambda *args, **kwargs: index_of_firstfile)
        assert TEST_FUNC(pytest.MOCKDIR) == dir_contents[index_of_firstfile].resolve()

    def test_subfolder_select(self, TEST_FUNC, monkeypatch):
        dir_contents = [path for path in pytest.MOCKDIR.iterdir()]
        dir_contents.sort()
        index_of_firstfolder = 0
        for index, path in enumerate(dir_contents):
            if path.is_dir():
                index_of_firstfolder = index
                break

        subdir = Path(dir_contents[index_of_firstfolder])
        subdir_contents = [path for path in  subdir.iterdir()]
        index_of_firstfile = 0
        for index, path in enumerate(subdir_contents):
            if path.is_file():
                index_of_firstfile = index
                break

        input_series = [index_of_firstfolder, index_of_firstfile]
        input_mock = Mock(side_effect=input_series, return_value=input_series)
        monkeypatch.setattr('builtins.input', input_mock)
        assert TEST_FUNC(pytest.MOCKDIR) == subdir_contents[index_of_firstfile].resolve()
        # Mock should have been called without having to do a manual test loop
        assert input_mock.call_count == len(input_series)