"""Module for requesting user inputted file/dir paths and subfolder navigation.

Helps avoid having to hard code paths into your scripts.
No third-party dependencies are required with the exception of pytest for the test scripts.
Returns paths as pathlib.Path objects.
"""
from pathlib import Path

def input_list_index(item_list, navigation_message=None):
    """Display provided list and return user index input selection."""

    navigation_message ='Select an option by entering the corresponding number.' if navigation_message is None else navigation_message
    
    for index, item in enumerate(item_list):
        print(f'{index}. {item}')

    item_index = input(navigation_message)

    try:
        item_index = int(item_index)
        if item_index in range(len(item_list)+1):
            return item_index
        else:
            raise(ValueError)
    except(ValueError):
        print(f'Invalid input. Please enter a number between 0 and {len(item_list)-1}\n')
        return input_list_index(item_list, navigation_message)

def input_path(criteria=None, path=None, input_message=None, navigation_message=None):
    """Returns given user path if criteria is fulfilled, otherwise launches file browsing interface by index.

    Arguments:
    criteria [function] -- a function that receives the input path as an arg and returns True/False. 
    Example criteria that validates folder paths only:
    def dironly_criteria(path):
        return Path(path).is_dir()

    input_message [str] -- descriptive message for [str] user input prompt eg. "Enter full path to JSON file."
    navigation_message -- descriptive message for [int] user input prompt during subfolder navigation eg. "Enter index of HTML file to parse."

    """
    def path_exists(path):
        return Path(path).exists()

    criteria = criteria if criteria is not None else path_exists
    input_message = input_message if input_message is not None else 'Enter path to item: \n'
    path = path if path is not None else input(input_message)

    if path is None or not Path(path).exists():
        print(f'No item exists at {path}.\n')
        return input_path(criteria, input_message=input_message, navigation_message=navigation_message)
    else:
        if criteria(path):
            return path
        else:
            custom_options = {
                "PARENT FOLDER": Path(path).parent,
                "CANCEL -- RE-ENTER FULL FILE PATH": lambda: input_path(criteria=criteria, input_message=input_message, navigation_message=navigation_message)
            }
            if Path(path).is_dir():
                print('Provided folder path does not fulfill criteria. Navigating folder for correct input...')
                return navigate_paths_by_index(Path(path).resolve(), custom_options, navigation_message=navigation_message)
            else:
                print('Provided file path does not fulfill criteria. Navigating parent folder for correct input....')
                return navigate_paths_by_index(Path(path).resolve().parent, custom_options, navigation_message=navigation_message)


def navigate_paths_by_index(dir_path, options_dict=None, navigation_message=None):
    """Interface lists subfolder and files of provided directory and returns path from user index seclection.
    Arguments:
    dir_path [str][required] -- Path to directory to be navigated.
    options_dict [dict] -- Dictionary keys are displayed as options, values are returned on selection.

    Returns:
    [str] Valid absolute path to a file. 

    If index to a subfolder provided, calls same method+options on subfolder to allow for navigation to valid file.

    """
    dir_path = Path(dir_path).resolve()
    dir_contents = [x for x in dir_path.iterdir()]
    print(f'\nAvailable files and subfolders in {dir_path.resolve()}:')

    if options_dict is None:
        options_dict = {}

    paths_display = []
    paths_values = []

    # Get sorted file and folder contents
    for path in dir_contents:
        if path.is_file():
            paths_display.append(path.name)
        elif path.is_dir():
            paths_display.append(f'/{path.name}/')

        paths_values.append(path.resolve())

    paths_display.sort()
    paths_display.extend(options_dict.keys())
    paths_values.sort()
    paths_values.extend(options_dict.values())

    input_index = input_list_index(item_list=paths_display, navigation_message=navigation_message)

    if isinstance(paths_values[input_index], Path):
        if paths_values[input_index].is_file():
            return paths_values[input_index]
        elif paths_values[input_index].is_dir():
            return navigate_paths_by_index(paths_values[input_index], navigation_message)
    else:
        return paths_values[input_index]()


def input_file_path(path=None, input_message=None, navigation_message=None):
    """Request and validate user input file path.

    Arguments:
    accept_dir [boolean] -- allows directory paths as well (default=True)

    Returns:
    [str] Valid absolute path to file/dir. Can be relative if searching in same dir as module.

    """
    def criteria(path):
        return Path(path).is_file()

    input_message = f'Enter full path of file to parse: ' if input_message is None else input_message
    path = path if path is not None else input_path(criteria, input_message=input_message, navigation_message=navigation_message)
    
    return path

def input_folder_path(path=None, input_message=None, navigation_message=None):
    """Request and validate user input file path.

    Arguments:
    accept_dir [boolean] -- allows directory paths as well (default=True)

    Returns:
    [str] Valid absolute path to file/dir. Can be relative if searching in same dir as module.

    """
    def criteria(path):
        return Path(path).is_file()

    input_message = f'Enter full path of file to parse: ' if input_message is None else input_message
    path = path if path is not None else input_path(criteria, input_message=input_message, navigation_message=navigation_message)
    
    return path